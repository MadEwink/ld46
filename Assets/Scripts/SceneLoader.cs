﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //SceneLoader needs a trigger collider in the scene.

    private int indexActiveScene;

    private void Start()
    {
        indexActiveScene = SceneManager.GetActiveScene().buildIndex;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("yeahh");
            indexActiveScene++;
            if (indexActiveScene < SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(indexActiveScene);
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
