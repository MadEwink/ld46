﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    protected Vector2 _position;
    protected float _rotationZ;
    public Vector2[] points;
    public Material material;

    protected LineRenderer _lineRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        
        _lineRenderer = gameObject.AddComponent<LineRenderer>();
        _lineRenderer.loop = true;
        _lineRenderer.startWidth = 0.05f;
        _lineRenderer.sortingOrder = 2;
        _lineRenderer.startColor = Color.white;
        _lineRenderer.material = material;
        UpdateLineRenderer();
    }

    protected void UpdateLineRenderer()
    {
        _lineRenderer.positionCount = points.Length;
        Vector3[] positions = new Vector3[points.Length];
        //_position is the position of the object. Points are the points which define the shape.
        _position = new Vector2(transform.position.x, transform.position.y);
        _rotationZ = transform.rotation.z;

        for (int i = 0; i < points.Length; i++)
        {
            Vector2 currentPosition = _position + points[i].x * (Vector2)transform.right + points[i].y * (Vector2)transform.up;
            positions[i] =new Vector3(currentPosition.x, currentPosition.y, 0);
        }
        _lineRenderer.SetPositions(positions);
    }

    // Update is called once per frame
    void Update()
    {
        //If the object moved or rotated, redraw it.
        if (_position.x != transform.position.x || _position.y != transform.position.y|| _rotationZ != transform.rotation.z)
            UpdateLineRenderer();
    }


    protected void OnDrawGizmos()
    {
        if (points.Length == 0)
            return;
        Gizmos.color = Color.white;
        Vector2 basePosition = new Vector2(transform.position.x, transform.position.y);
        for (int i = 0; i < points.Length-1; i++)
        {
            Gizmos.DrawLine(points[i + 1].x * (Vector2)transform.right + points[i+1].y * (Vector2)transform.up + basePosition, 
                points[i].x * (Vector2)transform.right + points[i].y * (Vector2)transform.up + basePosition);
        }

        Gizmos.DrawLine(points[0].x * (Vector2)transform.right + points[0].y * (Vector2)transform.up + basePosition, 
            points[points.Length-1].x * (Vector2)transform.right + (Vector2)transform.up * points[points.Length-1].y + basePosition);
    }
}