﻿using System;
using UnityEngine;

public class CameraBounds : Obstacle
{

    private Camera _camera;
    public void Start()
    {
        _camera = GetComponent<Camera>();
        Vector3 upLeft = _camera.ScreenToWorldPoint(new Vector2(0, 0));
        Vector3 upRight = _camera.ScreenToWorldPoint(new Vector2(Screen.width, 0));
        Vector3 downRight = _camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        Vector3 downLeft = _camera.ScreenToWorldPoint(new Vector2(0, Screen.height));
        points = new[]
        {
            new Vector2(upLeft.x, upLeft.y),
            new Vector2(upRight.x, upRight.y),
            new Vector2(downRight.x, downRight.y),
            new Vector2(downLeft.x, downLeft.y),
        };
    }
}
