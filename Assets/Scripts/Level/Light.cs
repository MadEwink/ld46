﻿using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Timeline;
using Vector2 = UnityEngine.Vector2;

public class Light : MonoBehaviour
{
    public bool baked = false;
    public bool needEpsilon = true;
    [Serializable]
    public class RayClass
    {
        public Vector2 origin;
        public Vector2 direction;
        public float t;


        public RayClass(Vector2 origin, Vector2 direction)
        {
            this.origin = origin;
            this.direction = direction;
            this.t = 0;
        }

        public bool Cast(RayClass segment, float length)
        {
            segment.t = (direction.x * (segment.origin.y - origin.y) - direction.y * (segment.origin.x - origin.x)) /
                        (direction.y * segment.direction.x - direction.x * segment.direction.y);
            if (Math.Abs(direction.x) > epsilon)
                t = (segment.origin.x + segment.direction.x * segment.t - origin.x) / direction.x;
            else if(Math.Abs(direction.y) > epsilon)
                t = (segment.origin.y + segment.direction.y * segment.t - origin.y) / direction.y;
            return segment.t >= -epsilon && segment.t <= length+epsilon && t >= -epsilon;
        }

        public void CastAll(Obstacle[] obstacles, float defaultMin = -1)
        {
            float min_t = defaultMin;
            foreach (var obstacle in obstacles)
            {
                Vector2 obstacleOrigin = new Vector2(obstacle.transform.position.x, obstacle.transform.position.y);
                for (int i = 0; i < obstacle.points.Length-1; i++)
                {
                    RayClass segment = new RayClass(obstacle.points[i].x * (Vector2)obstacle.transform.right + obstacle.points[i].y * (Vector2)obstacle.transform.up + obstacleOrigin,
                        (obstacle.points[i + 1].x * (Vector2)obstacle.transform.right +  obstacle.points[i+1].y * (Vector2)obstacle.transform.up
                        - obstacle.points[i].x * (Vector2)obstacle.transform.right - obstacle.points[i].y * (Vector2)obstacle.transform.up)
                        .normalized);
                    if ((direction == segment.direction) || !Cast(segment, (obstacle.points[i+1]-obstacle.points[i]).magnitude))
                        continue;
                    if (Math.Abs(min_t - (-1)) < epsilon || min_t > t)
                        min_t = t;
                }

                RayClass lastSegment = new RayClass(obstacle.points[obstacle.points.Length-1].x * (Vector2)obstacle.transform.right + obstacle.points[obstacle.points.Length - 1].y * (Vector2)obstacle.transform.up + obstacleOrigin,
                    (obstacle.points[0].x * (Vector2)obstacle.transform.right + obstacle.points[0].y * (Vector2)obstacle.transform.up 
                    - obstacle.points[obstacle.points.Length-1].x * (Vector2)obstacle.transform.right - obstacle.points[obstacle.points.Length - 1].y * (Vector2)obstacle.transform.up)
                    .normalized);
                if (Cast(lastSegment, (obstacle.points[0] - obstacle.points[obstacle.points.Length-1]).magnitude) && (min_t > t || Math.Abs(min_t - (-1)) < epsilon))
                    min_t = t;
            }

            t = min_t;
        }
    }
    
    public static float epsilon = 1E-05f;
    public bool GizmoRays = true;
    public bool GizmoLightShape = false;

    public List<RayClass> rays;

    public void ComputeRays(Obstacle[] obstacles)
    {
        Vector2 origin = new Vector2(transform.position.x, transform.position.y);
        rays = new List<RayClass>();
        foreach (var obstacle in obstacles)
        {
            Vector2 obstacleOrigin = new Vector2(obstacle.transform.position.x, obstacle.transform.position.y);
            foreach (var point in obstacle.points)
            {
                Vector2 direction = ((point.x * (Vector2)obstacle.transform.right + point.y * (Vector2)obstacle.transform.up + obstacleOrigin) - origin).normalized;
                rays.Add(new RayClass(origin, direction));
                rays[rays.Count-1].CastAll(obstacles);
                Vector2 rayNorm = new Vector2(direction.y, direction.x) * Mathf.Min(0.1f,500 * epsilon);
                direction = ((point.x * (Vector2)obstacle.transform.right + point.y * (Vector2)obstacle.transform.up + obstacleOrigin + rayNorm) - origin).normalized;
                
                if(needEpsilon)
                {
                    rays.Add(new RayClass(origin + rayNorm, direction));
                    rays[rays.Count - 1].CastAll(obstacles);

                    direction = ((point.x * (Vector2)obstacle.transform.right + point.y * (Vector2)obstacle.transform.up + obstacleOrigin - rayNorm) - origin).normalized;

                    rays.Add(new RayClass(origin - rayNorm, direction));
                    rays[rays.Count - 1].CastAll(obstacles);
                }
                
            }
        }
        RayComparer r = new RayComparer();
        rays.Sort(r);
    }

    public class RayComparer : IComparer<RayClass>
    {
        public int Compare(RayClass x, RayClass y)
        {
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null))
                return 0;
            else if (ReferenceEquals(x, null))
                return 1;
            else if (ReferenceEquals(y, null))
                return -1;
            float angle1 = Vector2.SignedAngle(Vector2.right, x.direction);
            float angle2 = Vector2.SignedAngle(Vector2.right, y.direction);
            if (angle1 < angle2)
                return -1;
            return 1;
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.5f);
        if (ReferenceEquals(rays,null) || rays.Count == 0)
            return;
        if (GizmoLightShape)
        {
            for (int i = 0; i < rays.Count; i++)
            {
                Gizmos.DrawLine(rays[i].origin + rays[i].direction*rays[i].t,
                    rays[(i+1)%rays.Count].origin+ rays[(i+1)%rays.Count].direction*rays[(i+1)%rays.Count].t);
            }
        }
        Gizmos.color = Color.red;
        if (GizmoRays)
        {
            foreach (var ray in rays)
            {
                Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * ray.t);
                Gizmos.DrawSphere(ray.origin + ray.direction * ray.t, 0.1f);
            }
        }
    }
}
