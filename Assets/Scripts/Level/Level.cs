﻿using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private List<Obstacle> obstacles;
    private List<Light> lights;
    [SerializeField]
    private SpriteRenderer LightPrefab;

    private Camera mainCamera;

    bool computeStatics = true;

    int currentLight = 0;
    int unbakedLights = 0;
    bool startComputing = true;
    private bool PointInPolygon(Vector2 point, List<Vector2> polygon)
    {
        int nbIntersection = 0;
        Light.RayClass ray = new Light.RayClass(point, Vector2.right);
        for (int i = 0; i < polygon.Count; i++)
        {
            Light.RayClass segment = new Light.RayClass(polygon[i],
                (polygon[(i+1)%polygon.Count] - polygon[i]).normalized);
            if (ray.Cast(segment, (polygon[(i + 1) % polygon.Count] - polygon[i]).magnitude))
                nbIntersection++;
        }

        return nbIntersection % 2 == 1;
    }

    public void Start()
    {
        obstacles = new List<Obstacle>();
        lights = new List<Light>();
        foreach (Transform child in transform)
        {
            Obstacle obstacle;
            if (child.TryGetComponent(out obstacle))
            {
                obstacles.Add(obstacle);
            }

            Light light;
            if (child.TryGetComponent(out light))
                lights.Add(light);
        }
        mainCamera = Camera.main;
        obstacles.Add(mainCamera.GetComponent<CameraBounds>());


        foreach (var light in lights)
        {
            if (!light.baked)
            {
                unbakedLights++;
            }
        }

    }

    private void CreateLightMasks()
    {
        int i = 0;
        int n = 0;
        foreach (var light in lights)
        {
            
            if(computeStatics || !light.baked)
            {
                n++;
                if(n==currentLight || computeStatics || true)
                {
                    Vector2 min = mainCamera.ScreenToWorldPoint(new Vector3(0, 0));
                    Vector2 max = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
                    Transform lightTransform = transform.Find("Light-" + i);
                    Sprite sprite;
                    PolygonCollider2D polygonCollider2D;
                    if (lightTransform)
                    {
                        sprite = lightTransform.GetComponent<SpriteRenderer>().sprite;
                        polygonCollider2D = lightTransform.GetComponent<PolygonCollider2D>();
                    }
                    else
                    {
                        GameObject newLightArea = new GameObject();
                        //SpriteRenderer spriteRenderer = Instantiate(LightPrefab);

                        SpriteRenderer spriteRenderer = newLightArea.AddComponent<SpriteRenderer>();


                        spriteRenderer.name = "Light-" + i;
                        spriteRenderer.transform.parent = transform;
                        Vector3 screenScale = new Vector3(max.x - min.x, max.y - min.y, 1);
                        spriteRenderer.transform.localScale = screenScale;

                        spriteRenderer.sprite = Sprite.Create(LightPrefab.sprite.texture,
                            new Rect(0, 0, LightPrefab.sprite.rect.width, LightPrefab.sprite.rect.height),
                            new Vector2(0.5f, 0.5f), LightPrefab.sprite.pixelsPerUnit);

                        spriteRenderer.material = LightPrefab.sharedMaterial;
                        spriteRenderer.color = LightPrefab.color;
                        /*
                        spriteRenderer.sprite = Sprite.Create(spriteRenderer.sprite.texture,
                            new Rect(0, 0, spriteRenderer.sprite.rect.width, spriteRenderer.sprite.rect.height),
                            new Vector2(0.5f,0.5f), spriteRenderer.sprite.pixelsPerUnit);
                            */
                        sprite = spriteRenderer.sprite;
                        polygonCollider2D = spriteRenderer.gameObject.AddComponent<PolygonCollider2D>();
                        polygonCollider2D.isTrigger = true;
                        spriteRenderer.gameObject.AddComponent<LightArea>();
                        spriteRenderer.gameObject.GetComponent<LightArea>().light = light.gameObject;
                    }
                    List<Vector2> colliderPath = new List<Vector2>();

                    light.ComputeRays(obstacles.ToArray());
                    List<Vector2> vertices = new List<Vector2>();
                    vertices.Add(light.rays[0].origin);
                    foreach (var ray in light.rays)
                    {
                        Vector2 point = ray.origin + ray.direction * ray.t;
                        if (point.x < min.x)
                            min.x = point.x;
                        if (point.y < min.y)
                            min.y = point.y;
                        if (point.x > max.x)
                            max.x = point.x;
                        if (point.y > max.y)
                            max.y = point.y;
                        //vertices.Add(new Vector2(point.x, point.y));
                        vertices.Add(point);
                    }
                    //Sprite sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0), 1);
                    /*
                    Transform spriteMaskTransform = transform.Find("LightMask"+i);
                    Sprite sprite;
                    if (spriteMaskTransform)
                        sprite = spriteMaskTransform.GetComponent<SpriteMask>().sprite;
                    else
                    {
                        SpriteMask spriteMask;
                        spriteMask = Instantiate(LightMaskPrefab);
                        spriteMask.name = "LightMask" + i;
                        spriteMask.transform.parent = transform;
                        Texture2D texture2D = Texture2D.normalTexture;
                        spriteMask.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height),
                            new Vector2(0.5f, 0), 100);
                        sprite = spriteMask.sprite;
                    }
                    */

                    polygonCollider2D.offset = new Vector2(-0.5f, -0.5f);

                    List<ushort> triangles = new List<ushort>();
                    for (int j = 0; j < vertices.Count; j++)
                    {
                        Vector2 vertex = vertices[j];
                        vertex -= min;
                        vertex.x /= (max.x - min.x);
                        vertex.y /= (max.y - min.y);
                        if (j != 0)
                            colliderPath.Add(vertex);
                        /*
                        else
                            polygonCollider2D.offset = vertex;
                            */
                        vertex.x *= sprite.rect.size.x;
                        vertex.y *= sprite.rect.size.y;
                        vertices[j] = vertex;
                        triangles.Add(0);
                        if (j == vertices.Count - 1)
                        {
                            triangles.Add((ushort)(j));
                            triangles.Add(1);
                        }
                        else
                        {
                            triangles.Add((ushort)((j + 1) % vertices.Count));
                            triangles.Add((ushort)((j + 2) % vertices.Count));
                        }
                    }
                    polygonCollider2D.SetPath(0, colliderPath);
                    sprite.OverrideGeometry(vertices.ToArray(), triangles.ToArray());

                }
            }
                
            i++;
        }
    }

    public void Update()
    {
        int n = 0;

        /*
        foreach (var light in lights)
        {
            if (computeStatics || !light.baked )
            {
                n++;
                if(computeStatics || n == currentLight)
                {
                    light.ComputeRays(obstacles.ToArray());
                    Debug.Log("A:" + light.GetInstanceID());
                }
            }
        }*/
        CreateLightMasks();
        currentLight = (currentLight + 1) % unbakedLights;
        computeStatics = false;
    }
}
