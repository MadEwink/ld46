﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




public struct PathPoint
{
    public float time;
    public Vector3 position;
    public Quaternion rotation;

    public PathPoint(float t, Vector3 pos, Quaternion rot)
    {
        time = t;
        position = pos;
        rotation = rot;
    }
}
public class PlayerController : MonoBehaviour
{
    private bool facingRight;

    [Header("Base stats")]
    [SerializeField]
    float baseSpeed=1;
    public float hp=100;
    public float maxHp = 100;
    public float lightAreaCounter = 0;

    [SerializeField]
    float gravityScaleInShadow=0;
    [SerializeField]
    float grabityScaleInLight=1;

    [SerializeField]
    float maxVelocityShadow;

    [SerializeField]
    float maxVelocityXLight;

    Vector2 target;
    public bool inShadow = true;

    Rigidbody2D rb2d;

    Rigidbody2D[] bodyRb2ds;

    [Header("Sprites")]
    [SerializeField]
    Sprite headShadow;
    [SerializeField]
    Sprite body1Shadow;
    [SerializeField]
    Sprite body2Shadow;
    [SerializeField]
    Sprite tailShadow;


    [SerializeField]
    Sprite headLight;
    [SerializeField]
    Sprite body1Light;
    [SerializeField]
    Sprite body2Light;
    [SerializeField]
    Sprite tailLight;

    [Header("Body")]
    [SerializeField]
    GameObject head;
    [SerializeField]
    GameObject body1;
    [SerializeField]
    GameObject body2;
    [SerializeField]
    GameObject body3;
    [SerializeField]
    GameObject tail;

    float time = 0;

    public float distanceMinBodyPart = 0.3f;
    public float distanceMaxBodyPart = 1;
    public float timeDeltaBodyPart = 0.1f;

    List<PathPoint> path = new List<PathPoint>();

    Vector3 headBaseScale;

    Vector3 body1BaseScale;

    Vector3 body2BaseScale;

    Vector3 body3BaseScale;

    Vector3 tailBaseScale;

    void Awake()
    {
        facingRight = true;

        rb2d = GetComponent<Rigidbody2D>();
        bodyRb2ds = GetComponentsInChildren<Rigidbody2D>();

        headBaseScale = head.transform.localScale;
        body1BaseScale = body1.transform.localScale;
        body2BaseScale = body2.transform.localScale;
        body3BaseScale = body3.transform.localScale;
        tailBaseScale = tail.transform.localScale;
        

    }

    private void ChangeSpriteOrientation(Vector3 target)
    {
        if (target.x < transform.position.x)
        {
            if (facingRight)
            {
                facingRight = !facingRight;
            }
        }
        else
        {
            if (!facingRight)
            {
                facingRight = !facingRight;
            }
        }
        head.GetComponent<SpriteRenderer>().flipY = !facingRight;

    }

    void Update()
    {
        time += Time.deltaTime;
        if(Input.GetMouseButton(0))
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            

            if (CheckIfShadow())
            {
                rb2d.velocity = (target - new Vector2(transform.position.x, transform.position.y)) * baseSpeed;
                if(rb2d.velocity.sqrMagnitude > maxVelocityShadow*maxVelocityShadow)
                {
                    rb2d.velocity = rb2d.velocity.normalized * maxVelocityShadow;
                }

            }
            else
            {
                float maxVelX = Mathf.Max(Mathf.Abs(rb2d.velocity.x), maxVelocityXLight);

                rb2d.velocity = new Vector2((target.x - transform.position.x) * baseSpeed, rb2d.velocity.y);
                if (Mathf.Abs(rb2d.velocity.x) > maxVelX)
                {
                    rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxVelX, rb2d.velocity.y);
                }
            }
            /*transform.LookAt(target, Vector3.left);
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z*Mathf.Rad2Deg);*/

            transform.rotation = Quaternion.Euler(0,0,
                Mathf.Atan2(target.y-transform.position.y, target.x - transform.position.x)*Mathf.Rad2Deg);





            //transform.rotation = Quaternion.Euler(0, 0, Vector2.Angle(Vector2.up, (Vector2)transform.position - target));
            ChangeSpriteOrientation(target);

        }


        if(!CheckIfShadow())
        {
            hp -= Time.deltaTime * 10f;

            foreach(SpriteRenderer sr in transform.parent.GetComponentsInChildren<SpriteRenderer>())
            {
                if(sr.material.HasProperty("_TransformationRatio"))
                {
                    sr.material.SetFloat("_TransformationRatio", 
                        Mathf.Min(1, sr.material.GetFloat("_TransformationRatio") + Time.deltaTime));
                }
            }
            
            if(hp> maxHp * 0.8f)
            {
                tail.transform.localScale = tailBaseScale * ( (hp-0.8f*maxHp) / (0.2f * maxHp));
            }
            else if (hp > maxHp * 0.6f)
            {
                tail.transform.localScale = Vector3.zero;
                body3.transform.localScale = body3BaseScale * ((hp - 0.6f * maxHp) / (0.2f * maxHp));
            }

            else if (hp > maxHp * 0.4f)
            {
                tail.transform.localScale = Vector3.zero;
                body3.transform.localScale = Vector3.zero;
                body2.transform.localScale = body2BaseScale * ((hp - 0.4f * maxHp) / (0.2f * maxHp));
            }
            else if (hp > maxHp * 0.2f)
            {
                tail.transform.localScale = Vector3.zero;
                body3.transform.localScale = Vector3.zero;
                body2.transform.localScale = Vector3.zero;
                body1.transform.localScale = body1BaseScale * ((hp - 0.2f * maxHp) / (0.2f * maxHp));
            }
            else if(hp>=0)
            {
                tail.transform.localScale = Vector3.zero;
                body3.transform.localScale = Vector3.zero;
                body2.transform.localScale = Vector3.zero;
                body1.transform.localScale = Vector3.zero;
                head.transform.localScale = headBaseScale * ( hp  / (0.2f * maxHp));
            }
            else
            {
                tail.transform.localScale = Vector3.zero;
                body3.transform.localScale = Vector3.zero;
                body2.transform.localScale = Vector3.zero;
                body1.transform.localScale = Vector3.zero;
                head.transform.localScale = Vector3.zero;
                GameOver();
            }
        }
        else
        {
            foreach (SpriteRenderer sr in transform.parent.GetComponentsInChildren<SpriteRenderer>())
            {
                if (sr.material.HasProperty("_TransformationRatio"))
                {
                    sr.material.SetFloat("_TransformationRatio", 
                        Mathf.Max(0,sr.material.GetFloat("_TransformationRatio") - Time.deltaTime));
                }
            }
        }
        path.Add(new PathPoint(time, transform.position, transform.rotation));

        /*
        Vector2 difference = body1.transform.position - head.transform.position;
        if (Vector2.SqrMagnitude(difference) >distanceMaxBodyPart*distanceMaxBodyPart)
        {
            body1.transform.position = (Vector2)head.transform.position + difference.normalized * distanceMaxBodyPart;
        }
        difference = body2.transform.position - body1.transform.position;
        if (Vector2.SqrMagnitude(difference) > distanceMaxBodyPart * distanceMaxBodyPart)
        {
            body2.transform.position = (Vector2)body1.transform.position + difference.normalized * distanceMaxBodyPart;
        }
        difference = body3.transform.position - body2.transform.position;
        if (Vector2.SqrMagnitude(difference) > distanceMaxBodyPart * distanceMaxBodyPart)
        {
            body3.transform.position = (Vector2)body2.transform.position + difference.normalized * distanceMaxBodyPart;
        }
        difference = tail.transform.position - body3.transform.position;
        if (Vector2.SqrMagnitude(difference) > distanceMaxBodyPart * distanceMaxBodyPart)
        {
            tail.transform.position = (Vector2)body3.transform.position + difference.normalized * distanceMaxBodyPart;
        }
        */

        PathPoint point = GetPathPoint(time - timeDeltaBodyPart);


        Vector2 difference = point.position - head.transform.position;
        if (point.time > 0 && difference.sqrMagnitude > distanceMinBodyPart * distanceMinBodyPart)
        {
            body1.transform.position = point.position;
            body1.transform.rotation = point.rotation;
        }
        point = GetPathPoint(time - 2 * timeDeltaBodyPart);
        difference = point.position - body1.transform.position;
        if (point.time > 0 && difference.sqrMagnitude > distanceMinBodyPart * distanceMinBodyPart)
        {
            body2.transform.position = point.position;
            body2.transform.rotation = point.rotation;
        }

        point = GetPathPoint(time - 3 * timeDeltaBodyPart);
        difference = point.position - body2.transform.position;
        if (point.time > 0 && difference.sqrMagnitude > distanceMinBodyPart * distanceMinBodyPart)
        {
            body3.transform.position = point.position;
            body3.transform.rotation = point.rotation;
        }

        point = GetPathPoint(time - 4 * timeDeltaBodyPart);
        difference = point.position - tail.transform.position;
        if (point.time > 0)
        {
            tail.transform.position = point.position;
            tail.transform.rotation = point.rotation;
        }


        if(path.Count> 10000)
        {
            var temp = path;
            path = new List<PathPoint>();
            for(int i=5000;i<temp.Count;i++)
            {
                path.Add(temp[i]);
            }
        }

    }




    bool CheckIfShadow()
    {
        return inShadow;
    }


    public void EnterLightArea()
    {
        lightAreaCounter++;
        inShadow = false;
        rb2d.gravityScale = grabityScaleInLight;
        foreach(Rigidbody2D rb in bodyRb2ds)
        {
            rb.gravityScale = grabityScaleInLight;
        }

        head.GetComponent<SpriteRenderer>().sprite = headLight;
        body1.GetComponent<SpriteRenderer>().sprite = body1Light;
        body2.GetComponent<SpriteRenderer>().sprite = body2Light;
        body3.GetComponent<SpriteRenderer>().sprite = body1Light;
        tail.GetComponent<SpriteRenderer>().sprite = tailLight;

    }

    public void QuitLightArea()
    {
        lightAreaCounter--;
        if (lightAreaCounter <= 0)
        {
            lightAreaCounter = 0;
            inShadow = true;
            rb2d.gravityScale = gravityScaleInShadow;
            foreach (Rigidbody2D rb in bodyRb2ds)
            {
                rb.gravityScale = gravityScaleInShadow;
            }

            head.GetComponent<SpriteRenderer>().sprite = headShadow;
            body1.GetComponent<SpriteRenderer>().sprite = body1Shadow;
            body2.GetComponent<SpriteRenderer>().sprite = body2Shadow;
            body3.GetComponent<SpriteRenderer>().sprite = body1Shadow;
            tail.GetComponent<SpriteRenderer>().sprite = tailShadow;
        }
    }

    void GameOver()
    {
        Debug.Log("GameOver");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    PathPoint GetPathPoint(float time)
    {
        if(path.Count==0)
        {
            return new PathPoint(-1,Vector3.zero, Quaternion.Euler(Vector3.zero));
        }
        int a = 0;
        int b = path.Count - 1;

        if(path[a].time > time)
        {
            return new PathPoint(-1, Vector3.zero, Quaternion.Euler(Vector3.zero));
        }
        while( b - a > 1)
        {
            int m = (a + b) / 2;
            if (path[m].time > time)
            {
                b = m;
            }
            else
            {
                a = m;
            }
        }
        return path[a];
    }
}
