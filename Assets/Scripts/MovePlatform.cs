﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    List<Vector3> targetPositions;
    public List<float> waitTimers;
    public bool willGoBackwards;
    int currentTargetIndex;
    int nextTargetIndex;



    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotateSpeed;

    private int sign = 1;

    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        targetPositions = new List<Vector3>();

        if(transform.childCount > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                targetPositions.Add(transform.GetChild(i).transform.position);
            }
            currentTargetIndex = 0;
            nextTargetIndex = 0;

            direction = targetPositions[nextTargetIndex] - transform.position;
            direction.Normalize();
        }
        else
        {
            currentTargetIndex = -1;
            nextTargetIndex = -1;
            direction = Vector3.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(-transform.forward, rotateSpeed);
        ///If there are targets:
        if (nextTargetIndex > -1)
        {
            if (Vector3.Distance(transform.position, targetPositions[nextTargetIndex]) < 0.1f)
            {
                StartCoroutine("WaitBeforeNextTarget");
            }

            ///Move towards target
            transform.position += Time.deltaTime * moveSpeed * direction;
            
        }
    }

    IEnumerator WaitBeforeNextTarget()
    {
        ///Stop movement
        direction = Vector3.zero;
        transform.position = targetPositions[nextTargetIndex];
        ///If the moving bit has to wait an amount of seconds on the target
        if (waitTimers[currentTargetIndex] >= 0)
        {
            if (willGoBackwards)
            {
                Debug.Log("will go backwards: current = " + currentTargetIndex);
                ///sign = 1 if it's going forward in the list, -1 if it's going backwards.
                nextTargetIndex = currentTargetIndex + 1 * sign;
                ///If it's the last target of the list:
                if (nextTargetIndex >= transform.childCount)
                {
                    nextTargetIndex -= 2;
                    sign *= -1;
                }
                if(nextTargetIndex < 0)
                {
                    sign *= -1;
                    nextTargetIndex += 2;
                }

                Debug.Log("and next is " + nextTargetIndex);

                ///Do the wait.
                float timer = 0.0f;

                while (timer < waitTimers[currentTargetIndex])
                {
                    yield return null;
                    timer += Time.deltaTime;
                }
            }

            else
            {
                //Debug.Log("will loop: current = " + currentTargetIndex);
                ///change the target.
                
                nextTargetIndex = currentTargetIndex + 1;
                ///If it's the last target of the list
                if (nextTargetIndex >= transform.childCount)
                {
                    nextTargetIndex = 0;
                }
                //Debug.Log("and next is " + nextTargetIndex);

                ///Do the wait.
                float timer = 0.0f;

                while (timer < waitTimers[currentTargetIndex])
                {
                    yield return null;
                    timer += Time.deltaTime;
                }

                
            }
            



            ///Change direction to point to the new target.
            direction = targetPositions[nextTargetIndex] - transform.position;
            direction.Normalize();

            ///Change the current target
            currentTargetIndex = nextTargetIndex;
        }
        ///If waitTimers[currentTargetIndex] is negative, then its the end of the line for the moving bit: change its target to -1.
        else
        {
            nextTargetIndex = -1;
            currentTargetIndex = -1;
        }
    }
}
