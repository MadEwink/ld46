﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightArea : MonoBehaviour
{
    public GameObject light;
    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("Player"))
        {
            collider2D.GetComponent<PlayerController>().EnterLightArea();
        }
    }

    void OnTriggerExit2D(Collider2D collider2D)
    {
        if (collider2D.CompareTag("Player"))
        {
            collider2D.GetComponent<PlayerController>().QuitLightArea();
        }
    }

    void Update()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        
        if (spriteRenderer != null && spriteRenderer.material.HasProperty("_Center") && light != null)
        {
            Vector2 lightposition = (Vector2)light.transform.position;
            Vector2 mininumSprite = spriteRenderer.bounds.min;
            Vector2 size = spriteRenderer.bounds.size;
            Vector2 center = new Vector2((lightposition.x - mininumSprite.x) / size.x, (lightposition.y - mininumSprite.y) / size.y);
            spriteRenderer.material.SetVector("_Center", center);
        }
    }

}
