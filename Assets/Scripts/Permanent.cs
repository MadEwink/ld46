﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Permanent : MonoBehaviour
{
    public static Permanent instanceAudio;
    // Start is called before the first frame update
    void Start()
    {
        if (instanceAudio)
        {
            GameObject.Destroy(this.gameObject);
        }
        else
        {
            instanceAudio = this;
        }

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
