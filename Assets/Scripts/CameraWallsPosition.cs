﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWallsPosition : MonoBehaviour
{
    public Camera mainCamera;
    float height;
    float width;
    [SerializeField] float offset;

    Vector3 center;
    public Transform wallUp;
    public Transform wallLeft;
    public Transform wallDown;
    public Transform wallRight;

    // Start is called before the first frame update
    void Start()
    {
        center = mainCamera.transform.position;
        height = 2 * mainCamera.orthographicSize / mainCamera.aspect;
        width = 2 * mainCamera.orthographicSize;

        wallUp.position = center + Vector3.up * (height + offset);
        wallUp.localScale = new Vector3(2 *width, 1, 0);

        wallDown.position = center - Vector3.up * (height + offset);
        wallDown.localScale = new Vector3(2 * width, 1, 0);

        wallLeft.position = center - Vector3.right * (width + offset);
        wallLeft.localScale = new Vector3(1, 2 * height, 0);

        wallRight.position = center + Vector3.right * (width + offset);
        wallRight.localScale = new Vector3(1, 2 * height, 0);
    }

}
