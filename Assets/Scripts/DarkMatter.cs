﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkMatter : MonoBehaviour
{

    SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    void OnTriggerStay2D(Collider2D collider2D)
    {
        if (collider2D.CompareTag("Player"))
        {
            
            Vector2 playerPos = collider2D.transform.position;

            Vector2 min = spriteRenderer.bounds.min;
            Vector2 size = spriteRenderer.bounds.size;
            
            Vector2 center = new Vector2((playerPos.x - min.x) / size.x,  (playerPos.y - min.y) / size.y);
            spriteRenderer.material.SetVector("_ShearCenter", center);
        }
    }
    void OnTriggerExitD(Collider2D collider2D)
    {
        if (collider2D.CompareTag("Player"))
        {
            Vector2 center = new Vector2(-100,-100);

            spriteRenderer.material.SetVector("_ShearCenter", center);
        }
    }
}
